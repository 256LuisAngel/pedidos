<?php
$app->get('/','FrontEnd::indexAction')->bind('raiz');
$app->get('/registro','FrontEnd::registro')->bind('portada');
$app->post('/acceso','FrontEnd::clienteRegistrado')->bind('Cliente registrado');
$app->get('/productos','FrontEnd::catalogoProductos')->bind('Catalogo productos');
$app->get('/login','FrontEnd::login')->bind('Inicio de sesion');
$app->post('/login','FrontEnd::loginValidacion')->bind('Sesion Iniciada');
$app->get('/intermedia','FrontEnd::intermedia')->bind('intermedia');
$app->get('/producto/{sku}','FrontEnd::producto')->bind('Seleccion producto');
$app->post('/agregar/{sku}','FrontEnd::agregar')->bind('Agregar producto');
$app->get('/intermedia2','FrontEnd::intermedia2')->bind('intermedia2');